<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Session;

class HomeController extends Controller
{
    public function index(){
        $token = Session::get('token');
        if($token == ''){
            return redirect('/login'); 
        } else{

            $username = Session::get('username');
            $data = Http::withToken($token)->get("http://52.77.253.103:3000/api/v1/activities")
            ->json();
    
            return view('index', compact('data'));
        }
    }

    public function logout(){
        $token = Session::get('token');
        if($token == ''){
            return redirect('home');
        } else{
            session()->forget('token');
            session()->forget('username');
            return redirect('/login');
        }
    }
}
